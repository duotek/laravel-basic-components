<?php

namespace Duotek\LaravelBasicComponents\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * Duotek\LaravelBasicComponents\Models;
 *
 * @property int $id
 * @property string $title
 * @property string $ident
 * @property string $filters
 *
 * @method static Builder|BrowserFilterPreset query()
 *
 * @mixin Builder
 */
class BrowserFilterPreset extends Model
{
    protected $table = 'duotek_browser_filters_presets';

    public $timestamps = false;
}
