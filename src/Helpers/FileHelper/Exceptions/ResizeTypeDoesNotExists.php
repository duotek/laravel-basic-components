<?php

namespace Duotek\LaravelBasicComponents\Helpers\FileHelper\Exceptions;

use Exception;

class ResizeTypeDoesNotExists extends Exception {}
