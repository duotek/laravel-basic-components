<?php

namespace Duotek\LaravelBasicComponents\Service\PanelSetSortableUpdateService\Exceptions;

use Exception;

class PassedClassDoesNotInheritPanelSetSortable extends Exception {}