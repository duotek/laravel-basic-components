<?php

namespace Duotek\LaravelBasicComponents\Service\PanelSetSortableUpdateService\Exceptions;

use Exception;

class FailedToCreateAnInstanceFromThePassedClass extends Exception {}