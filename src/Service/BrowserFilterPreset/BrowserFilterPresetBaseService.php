<?php

namespace Duotek\LaravelBasicComponents\Service\BrowserFilterPreset;

use Duotek\LaravelBasicComponents\Service\Service;

abstract class BrowserFilterPresetBaseService extends Service
{
    static string $browserFilterPresetModel = 'Duotek\\LaravelBasicComponents\\Models\\BrowserFilterPreset';
}