﻿<?php

namespace Duotek\LaravelBasicComponents\Traits\Model\UploadFile\Exceptions;

use Exception;

class InvalidFilePropertiesWithSettingsPropertyConfiguration extends Exception {}
