<?php

namespace Duotek\LaravelBasicComponents\PanelForm\Exceptions;

use Exception;

class InvalidPanelFormConfigurationException extends Exception {}