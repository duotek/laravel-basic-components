<?php

namespace Duotek\LaravelBasicComponents\PanelSet\Filters;

class SelectFilter extends BaseSelectFilter
{
    public function getType(): string
    {
        return 'SELECT';
    }
}
