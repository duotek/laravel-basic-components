<?php

namespace Duotek\LaravelBasicComponents\PanelSet\Exceptions;

use Exception;

class InvalidJsonFormatForFiltersParameterException extends Exception {}