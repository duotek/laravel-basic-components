<?php

namespace Duotek\LaravelBasicComponents\PanelSet\Exceptions;

use Exception;

class InvalidPanelSetConfigurationException extends Exception {}