<?php

namespace Duotek\LaravelBasicComponents\PanelSet\Exceptions;

use Exception;

class InvalidOptionsFormatException extends Exception {}